package com.example.exam.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.exam.core.resource.Resource
import com.example.exam.data.repository1.repository
import com.example.exam.models.goldAndSilver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ViewModelForTala(): ViewModel() {
    val goldAndSilverResult = MutableLiveData<Resource<goldAndSilver>>()
    val goldAndSilverRepository = repository()
    fun getGoldAndSilver(){
        goldAndSilverResult.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            goldAndSilverResult.postValue(
                goldAndSilverRepository.getGoldAndSilver()
            )
        }
    }
}
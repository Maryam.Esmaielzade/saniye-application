package com.example.exam.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.exam.core.resource.Resource
import com.example.exam.data.repository1.repository
import com.example.exam.models.namadArchive
import com.example.exam.models.namadLatest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ViewModelForOneNamad(): ViewModel() {

    val namadResult = MutableLiveData<Resource<namadArchive>>()
    val namadRepository = repository()
    fun getNamadArchive(){
        namadResult.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            namadResult.postValue(
                namadRepository.getNamadArchive()
            )
        }
    }
}

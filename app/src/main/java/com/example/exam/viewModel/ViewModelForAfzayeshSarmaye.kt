package com.example.exam.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.exam.core.resource.Resource
import com.example.exam.data.repository1.repository
import com.example.exam.models.stockGain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ViewModelForAfzayeshSarmaye(): ViewModel() {

    val stockGainResult = MutableLiveData<Resource<stockGain>>()
    val stockGainRepository = repository()
    fun getStockGainWithID(){
        stockGainResult.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            stockGainResult.postValue(
                stockGainRepository.getStockGainWithID()
            )
        }
    }
}
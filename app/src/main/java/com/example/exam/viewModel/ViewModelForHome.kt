package com.example.exam.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.exam.core.resource.Resource
import com.example.exam.data.repository1.repository
import com.example.exam.models.*
//import com.example.exam.models.All
//import com.example.exam.models.AllCountries
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ViewModelForHome(): ViewModel() {
//    val result = MutableLiveData<Resource<AllCountries>>()
//    val result1 = MutableLiveData<Resource<All>>()
    val namadResult = MutableLiveData<Resource<namadLatest>>()
    val namadRepository = repository()
    val namadItemResult = MutableLiveData<Resource<namadLatestItem>>()
    val namadItemRepository = repository()
    val shakhesResult = MutableLiveData<Resource<shakhes>>()
    val shakhesRepository = repository()
    var id:String=""

//    val countryRepository = repository()
//
//    fun getAll() {
//        result1.value = Resource.loading(null)
//
//        viewModelScope.launch(Dispatchers.IO) {
//            result1.postValue(
//                countryRepository.getAll()
//            )
//        }
//    }
//    fun getAllCountries() {
//        result.value = Resource.loading(null)
//
//        viewModelScope.launch(Dispatchers.IO) {
//            result.postValue(
//                countryRepository.getAllCountries()
//            )
//        }
//    }
    fun getNamadLatest(){
        namadResult.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            namadResult.postValue(
                namadRepository.getNamadLatest()
            )
        }
    }
    fun getNamadLatestWithID(id:String){
        namadItemResult.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            namadItemResult.postValue(
                namadItemRepository.getNamadLatestWithID(id)
            )
        }
    }
    fun getShakhes(){
        shakhesResult.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            shakhesResult.postValue(
                shakhesRepository.getShakhes()
            )
        }
    }
}
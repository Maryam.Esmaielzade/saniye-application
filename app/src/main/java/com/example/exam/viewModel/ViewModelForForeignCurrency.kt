package com.example.exam.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.exam.core.resource.Resource
import com.example.exam.data.repository1.repository
import com.example.exam.models.ForeignCurrency
import com.example.exam.models.arzDigital
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ViewModelForForeignCurrency :ViewModel() {
    val foreignCurrencyResult = MutableLiveData<Resource<ForeignCurrency>>()
    val foreignCurrencyRepository = repository()
    fun getForeignCurrency(){
        foreignCurrencyResult.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            foreignCurrencyResult.postValue(
                foreignCurrencyRepository.getForeignCurrency()
            )
        }
    }
}
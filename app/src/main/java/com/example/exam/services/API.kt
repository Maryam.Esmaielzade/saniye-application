package com.example.exam.services

//import com.example.exam.models.All
//import com.example.exam.models.AllCountries
//import com.example.exam.models.SpecificCountry
import com.example.exam.models.*
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Query

interface API {
//    @GET("all")
//    suspend fun getAll(): All
//
//    @GET("countries/:query")
//    suspend fun getSpecificCountry(): SpecificCountry
//
//    @GET("countries")
//    suspend fun getAllCountries(): AllCountries

    @GET("/api/namadLatest")
    suspend fun getNamadLatest(): namadLatest
    @GET("/api/namadArchive")
    suspend fun getNamadArchive(): namadArchive
    @GET("/api/shakhes")
    suspend fun getShakhes():shakhes
    @GET("/api/goldAndSilver")
    suspend fun getGoldAndSilver():goldAndSilver
    @GET("/api/arzDigital")
    suspend fun getArzDigital(): arzDigital
    @GET("/api/foreignCurrency")
    suspend fun getForeignCurrency(): ForeignCurrency

    @GET("/api/namadLatest")
    suspend fun getNamadLatestWithID(@Body id:String):namadLatestItem

    @GET("/api/stockGain")
    suspend fun getStockGainWithID():stockGain
}
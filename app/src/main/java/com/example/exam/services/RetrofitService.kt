package com.example.exam.services

import androidx.lifecycle.MutableLiveData
import com.example.exam.MyApp

import com.facebook.flipper.plugins.network.FlipperOkhttpInterceptor
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {
    val responseStatus = MutableLiveData<Response>()

    private val client = OkHttpClient.Builder()
        .addNetworkInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val proceed = chain.proceed(request = chain.request())
                responseStatus.postValue(proceed)
                return proceed
            }
        })

        .addNetworkInterceptor(FlipperOkhttpInterceptor(MyApp.networkFlipperPlugin))
        .build()
    val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("http://192.168.1.106:81")
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()
}
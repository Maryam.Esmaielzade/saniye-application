package com.example.exam.storage.data

import android.content.SharedPreferences
import com.example.exam.storage.types.IntPreference
import com.example.exam.storage.types.StringPreference



class Settings(sharedPreferences: SharedPreferences) {

    var authToken: String? by StringPreference(
        sharedPreferences,
        AUTH_TOKEN
    )
    var username: String? by StringPreference(
        sharedPreferences,
        USER_NAME
    )
    var articleFacCommSlug: String? by StringPreference(
        sharedPreferences,
        ArticlesFavComm_SLUG
    )
    var articlesCount: Int? by IntPreference(
        sharedPreferences,
        Articles_Count
    )

    companion object Key {
        const val AUTH_TOKEN = "AUTH_TOKEN"
        const val USER_NAME = "USER_NAME"
        const val ArticlesFavComm_SLUG = "ArticlesFavComm_SLUG"
        const val Articles_Count = "Articles_Count"
    }
}
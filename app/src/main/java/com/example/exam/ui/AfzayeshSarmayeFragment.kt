package com.example.exam.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.exam.R
import com.example.exam.core.resource.Status
import com.example.exam.databinding.FragmentAfzayeshSarmayeBinding
import com.example.exam.models.StockGainItem
import com.example.exam.models.namadLatestItem
import com.example.exam.viewModel.ViewModelForAfzayeshSarmaye
import kotlinx.android.synthetic.main.fragment_home.*


class AfzayeshSarmayeFragment : Fragment() {
    lateinit var viewModel: ViewModelForAfzayeshSarmaye
//    var namadAS: List<AfzayeshSarmayeItems>? = null
    val AS= mutableListOf<AfzayeshSarmayeItems>()
    lateinit var binding: FragmentAfzayeshSarmayeBinding
    val args by navArgs<AfzayeshSarmayeFragmentArgs>()
    private val ASadapter=AfzayeshSarmayeAdapter(
        this,
        AS,
        this,
        object:MenuClick{
            override fun onItemClick(country: String) {
                TODO("Not yet implemented")
            }
        }
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getStockGainWithID()


        viewModel.stockGainResult.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS) {
                recyclerView.apply {
                    var flag=0
                for(i in 0 until (it.data?.size!!)){
                if(it.data?.get(i)?.symbolId==args.namad){
                binding.namad= it.data?.get(0)?.symbol ?:""
                binding.price= it.data?.get(0)?.todayPrice.toString()

//                    if (i > 0) {
//                        println("@@@@@@@" + i)
                    AS.add(
                        AfzayeshSarmayeItems(
                            it.data?.get(i)?.raisingStockDate,
                            it.data?.get(i)?.beforeRaisingStockPrice,
                            it.data?.get(i)?.afterRaisingStockPrice
                        )
                    )
                    flag=1
//                break
                }
            }
                if(flag==0){
                    binding.namad= "در دو سال گذشته افزایش سرمایه ای برای این نماد ثبت نشده است"
                }

                    adapter = ASadapter
                    val manager4 = layoutManager
                    recyclerView.layoutManager = manager4
                    setHasFixedSize(true)
                }
            }
        }
        )
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_afzayesh_sarmaye, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ViewModelForAfzayeshSarmaye::class.java)
        binding.also {
            it.vm = viewModel
            it.lifecycleOwner = viewLifecycleOwner
        }
    }

    companion object {
    }
}
package com.example.exam.ui

data class items (
        var name: String?, var todayPrice: String?, var unit:String?, var changePrice: String?, var changePricePercent: String?,
        var date:String? , var time: String?, var LorH:Boolean, var star: Boolean?
    )

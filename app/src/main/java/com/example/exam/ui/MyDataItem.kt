package com.example.exam.ui

data class MyDataItem (
    var name: String?, var todayPrice: Int?, var changePrice: Int?, var date:String?, var star: Boolean?, var id:String?
)

package com.example.exam.ui

//import com.example.exam.models.AllCountriesItem
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.exam.R
import com.example.exam.core.resource.Status
import com.example.exam.data.repository1.repository
import com.example.exam.databinding.FragmentHomeBinding
import com.example.exam.models.namadLatestItem
import com.example.exam.models.shakhes
import com.example.exam.viewModel.ViewModelForHome
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment() , MarkListener<MyDataItem>{
    var namads: List<namadLatestItem>? = null
    var shakhes: shakhes? = null
    lateinit var binding: FragmentHomeBinding
    lateinit var viewModel: ViewModelForHome
    val namadList= mutableListOf<MyDataItem>()
    lateinit var id:String
    private val madapter=MyAdapter(
        this,
        namadList,
        this,
        object:MenuClick{
            override fun onItemClick(namad: String) {
                val action =HomeFragmentDirections.actionHomeFragmentToAfzayeshSarmayeFragment(namad)
                findNavController().navigate(action)
            }
        }
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


//        viewModel.getAllCountries()
//        viewModel.getAll()
//        viewModel.result1.observe(viewLifecycleOwner, Observer {
//            println(it.data)
//            if(it.data==null){
//                println(it.message)
//            }
//        })

//        tabs.newTab().setText("سهام")
//        tabs.newTab().setText("افزایش سرمایه ها")
//        tabs.newTab().setText("طلا و نقره")
//        tabs.newTab().setText("ارز")
//        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
//            override fun onTabSelected(tab: TabLayout.Tab) {
//                if (tab.position == 0) {
        viewModel.getNamadLatest()
        viewModel.getShakhes()
//        binding.search.requestFocus()
//        go.setOnClickListener{
//            viewModel.id= binding.search.text.toString()
//            viewModel.getNamadLatestWithID(viewModel.id)
//        }
//        viewModel.getNamadLatestWithID("408934423224097")
//        binding.search.requestFocus()
        viewModel.shakhesResult.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS) {
                shakhes = it.data
                binding.kol= shakhes?.shakhesKol ?: null
                binding.hamVazn= shakhes?.shakhesHamVazn ?:null
                binding.arzeshBazar = shakhes?.arzeshBazar ?: null
                binding.rooz = shakhes?.date ?: null
            }
        }
        )
        viewModel.namadResult.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS) {
                namads = it.data
                recyclerView.apply {
                    if (namadList.isEmpty()) {
                        for (i in 1 until 10) {
                            if (i > 0) {
                                println("@@@@@@@" + i)
                                var differ =
                                    namads?.get(i)?.finalPrice!! / namads?.get(i)?.yesterdayPrice!! * 100
                                namadList.add(
                                    MyDataItem(
                                        namads?.get(i)?.symbol,
                                        namads?.get(i)?.lastTransactionPrice,
                                        differ,
                                        namads?.get(i)?.date,
                                        false,
                                        namads?.get(i)?.symbolId
                                    )
                                )
                            }
                        }
                    }
                    go.setOnClickListener {
                        for (i in 1 until 10) {
                            namadList.remove(MyDataItem(
                                namads?.get(i)?.symbol,
                                namads?.get(i)?.lastTransactionPrice,
                                namads?.get(i)?.finalPrice!! / namads?.get(i)?.yesterdayPrice!! * 100,
                                namads?.get(i)?.date,
                                false,
                                namads?.get(i)?.symbolId
                            ))
                            if (search.text?.equals(namads?.get(i)?.symbol) == true){
                                namadList.add(
                                    MyDataItem(
                                        namads?.get(i)?.symbol,
                                        namads?.get(i)?.lastTransactionPrice,
                                        namads?.get(i)?.finalPrice!! / namads?.get(i)?.yesterdayPrice!! * 100,
                                        namads?.get(i)?.date,
                                        false,
                                        namads?.get(i)?.symbolId
                                    )
                                )
                            }
                        }
                    }
                    adapter = madapter
                    val manager2 = layoutManager
                    recyclerView.layoutManager = manager2
                    setHasFixedSize(true)

                }
            }
        }
        )
//        }
//                if(tab.position==1){
//
//                }
//                if(tab.position==2){
//                    viewModel.getGoldAndSilver()
//
//                }
//                if(tab.position==3){
//                    viewModel.getArzDigital()
//                }
//                }
//
//                override fun onTabUnselected(tab: TabLayout.Tab) {
//
//                }
//
//                override fun onTabReselected(tab: TabLayout.Tab) {
//
//                }
//            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val repository = repository()
        viewModel = ViewModelProvider(this).get(ViewModelForHome::class.java)
        binding.also {
            it.vm = viewModel
            it.lifecycleOwner = viewLifecycleOwner
        }
    }

    override fun onClick(myDataItem: MyDataItem, layoutPosition: Int) {
        madapter.notifyItemChanged(layoutPosition)
    }


}

package com.example.exam.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.exam.R
import kotlinx.android.synthetic.main.row_my_item.view.*


interface MenuClick {
    fun onItemClick(name: String)
}

class MyAdapter(
    val context: HomeFragment,
    val list: MutableList<MyDataItem>,
    var markListener: HomeFragment? = null,
    var menuListener: MenuClick? = null
) :
    RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

     inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var price:TextView=itemView.todayPrice
        var name:TextView=itemView.namadName
        var change:TextView=itemView.changePrice
         var date:TextView=itemView.namadDate
//        var image : ImageView = itemView.Image
        init{
            itemView.itemClick.setOnClickListener() {
                list[layoutPosition]?.id.let { it1 ->
                    if (it1 != null) {
                        menuListener?.onItemClick(it1)
                    }
                }

            }
            itemView.star.setOnClickListener {
                markListener?.onClick(list[layoutPosition], layoutPosition)
                if (list[layoutPosition].star!!) {
                    itemView.star.setImageDrawable(
                        context.resources.getDrawable(R.drawable.ic_baseline_star_border_24)
                    )
                    list[layoutPosition].star = false
                } else {
                    itemView.star.setImageDrawable(
                    context.resources.getDrawable(R.drawable.ic_baseline_star_24)
                    )
                    list[layoutPosition].star = true
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        // create a new view
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_my_item, parent, false)
        return MyViewHolder(view)

    }

    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.price.text = list[position].todayPrice.toString()
        holder.change.text = list[position].changePrice.toString()
        holder.name.text = list[position].name
        holder.date.text = list[position].date
//        holder.image.setImageDrawable(context.resources.getDrawable(R.drawable.ic_launcher_foreground))
    }

}

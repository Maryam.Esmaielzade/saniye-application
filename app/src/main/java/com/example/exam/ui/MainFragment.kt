package com.example.exam.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.exam.R
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_main.*


class MainFragment : Fragment() {
    var coursesGV: GridView? = null
    val courseModelList = mutableListOf<CourseModel>()
    private val cadapter=CourseGVAdapter(
        this,
        courseModelList,
        object : cardItemClick {
            override fun onItemClick(name: String?) {
                if (name=="سهام"){
                    val action = MainFragmentDirections.actionMainFragmentToHomeFragment()
                    findNavController().navigate(action)
                }
                if (name=="ارز دیجیتال"){
                    val action = MainFragmentDirections.actionMainFragmentToArzDigitalFragment()
                    findNavController().navigate(action)
                }
                if (name=="سکه"){
                    val action = MainFragmentDirections.actionMainFragmentToSekkeFragment()
                    findNavController().navigate(action)
                }
                if (name=="طلا"){
                    val action = MainFragmentDirections.actionMainFragmentToTalaONoghreFragment()
                    findNavController().navigate(action)
                }
                if (name=="نقره"){
                    val action = MainFragmentDirections.actionMainFragmentToNoghreFragment()
                    findNavController().navigate(action)
                }
                if (name=="ارز"){
                    val action = MainFragmentDirections.actionMainFragmentToDollarFragment()
                    findNavController().navigate(action)
                }
            }
        }
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //idGVcourses.apply {
        recycler.apply{
            if (courseModelList.isEmpty()) {
                courseModelList.add(CourseModel("سهام", R.drawable.bourse))
                courseModelList.add(CourseModel("ارز دیجیتال", R.drawable.bitcoin_))
                courseModelList.add(CourseModel("سکه", R.drawable.coin))
                courseModelList.add(CourseModel("طلا", R.drawable.gold_))
                courseModelList.add(CourseModel("نقره", R.drawable.silver_coins_bar))
                courseModelList.add(CourseModel("ارز", R.drawable.dollar))
            }
                adapter = cadapter
                val layoutManager = AutoFitGridLayoutManager(context, 500)
                recycler.layoutManager = layoutManager
                setHasFixedSize(true)

        }
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    companion object {

    }
    fun onClick(myDataItem: CourseModel, layoutPosition: Int) {
        cadapter.notifyItemChanged(layoutPosition)
    }
}
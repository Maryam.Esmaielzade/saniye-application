package com.example.exam.ui

interface MarkListener<T> {
    fun onClick(myDataItem: T, layoutPosition: Int)
}
package com.example.exam.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.card_item.view.*
import kotlinx.android.synthetic.main.row_my_item.view.*
import kotlinx.android.synthetic.main.rv_items.view.*


interface cardItemClick {
    fun onItemClick(name: String?)
}
class CourseGVAdapter(
    context: MainFragment,
    val list: MutableList<CourseModel>,
    var menuListener: cardItemClick? = null
) :
    RecyclerView.Adapter<CourseGVAdapter.MyViewHolder>() {
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name :TextView=itemView.idTVCourse
        var courseIV :ImageView= itemView.idIVcourse

        init{
            itemView.firstClick.setOnClickListener(){
                menuListener?.onItemClick(list[layoutPosition].course_name)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(com.example.exam.R.layout.card_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount()= list.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.name.text = list[position].course_name
        holder.courseIV.setImageResource(list[position].imgid)
    }
}

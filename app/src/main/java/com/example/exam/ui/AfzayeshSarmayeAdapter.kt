package com.example.exam.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.exam.R
import com.example.exam.models.StockGainItem
import kotlinx.android.synthetic.main.afzayesh_sarmaye_items.view.*
import kotlinx.android.synthetic.main.rv_items.view.*

class AfzayeshSarmayeAdapter(
    val context: AfzayeshSarmayeFragment,
    val list: MutableList<AfzayeshSarmayeItems>,
    var markListener: AfzayeshSarmayeFragment? = null,
    var menuListener: MenuClick? = null
) :
    RecyclerView.Adapter<AfzayeshSarmayeAdapter.MyViewHolder>() {

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var date: TextView =itemView.namadDate
        var before: TextView =itemView.before
        var after : TextView = itemView.after

        //        var image : ImageView = itemView.Image
        init{

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        // create a new view
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.afzayesh_sarmaye_items, parent, false)
        return MyViewHolder(view)

    }

    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.date.text = list[position].today.toString()
        holder.before.text = list[position].before.toString()
        holder.after.text = list[position].after.toString()

//        holder.image.setImageDrawable(context.resources.getDrawable(R.drawable.ic_launcher_foreground))
    }


}
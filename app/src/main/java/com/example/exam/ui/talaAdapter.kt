package com.example.exam.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.exam.R
import kotlinx.android.synthetic.main.rv_items.view.*


class talaAdapter(
    val context: TalaONoghreFragment,
    val list: MutableList<items>,
    var markListener: TalaONoghreFragment? = null,
    var menuListener: MenuClick? = null
) :
    RecyclerView.Adapter<talaAdapter.MyViewHolder>() {

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView =itemView.name
        var price: TextView =itemView.price
        var unit : TextView = itemView.unit
        var change: TextView =itemView.change
        var changePercent :TextView= itemView.changePercent
        var date :TextView= itemView.date
        var time :TextView= itemView.time

        //        var image : ImageView = itemView.Image
        init{
            itemView.Click.setOnClickListener() {
                list[layoutPosition].name?.let { it1 -> menuListener?.onItemClick(it1) }

            }
            itemView.Image.setOnClickListener {
                markListener?.onClick(list[layoutPosition], layoutPosition)
                if (list[layoutPosition].star!!) {
                    itemView.Image.setImageDrawable(
                        context.resources.getDrawable(R.drawable.ic_baseline_star_border_24)
                    )
                    list[layoutPosition].star = false
                } else {
                    itemView.Image.setImageDrawable(
                        context.resources.getDrawable(R.drawable.ic_baseline_star_24)
                    )
                    list[layoutPosition].star = true
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        // create a new view
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.rv_items, parent, false)
        return MyViewHolder(view)

    }

    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.name.text = list[position].name.toString()
        holder.price.text = list[position].todayPrice.toString()
        holder.unit.text = list[position].unit
        holder.change.text = list[position].changePrice.toString()
        holder.changePercent.text = list[position].changePricePercent.toString()
        holder.date.text = list[position].date.toString()
        holder.time.text = list[position].time
//        holder.image.setImageDrawable(context.resources.getDrawable(R.drawable.ic_launcher_foreground))
    }

}
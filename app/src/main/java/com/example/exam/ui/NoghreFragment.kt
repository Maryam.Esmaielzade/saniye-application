package com.example.exam.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.exam.R
import com.example.exam.core.resource.Status
import com.example.exam.viewModel.ViewModelForTala
import kotlinx.android.synthetic.main.fragment_home.*

class NoghreFragment : Fragment() {
//    var star= mutableListOf<Boolean>(false,false,false,false,false)
    lateinit var viewModel: ViewModelForTala
//    var silver925: Silver925? = null
//    var silver999: Silver999? = null
    val noghre= mutableListOf<items>()
    //    var gold: List<namadLatestItem>? = null
    private val nadapter=NoghreAdapter(
        this,
        noghre,
        this,
        object:MenuClick{
            override fun onItemClick(country: String) {
                TODO("Not yet implemented")
            }
        }
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getGoldAndSilver()
//        viewModel.goldAndSilverResult.observe(viewLifecycleOwner, Observer {
//            if (it.status == Status.SUCCESS) {
//                binding.nameValueOns="انس طلای جهانی"
//                binding.priceValueOns= it.data?.ons?.p ?: null
//                binding.dateValueOns= it.data?.ons?.date ?: null
//                binding.changeValueOns= it.data?.ons?.d ?: null
//                binding.changePercentValueOns= "("+it.data?.ons?.dp.toString()+")"
//                if(it.data?.ons?.dt ?:null =="high"){
//                    binding.lorHOns==true
//                } else if(it.data?.ons?.dt ?:null =="low"){
//                    binding.lorHOns==false
//                }
//            }
//        }
//        )


//        view?.Image?.setOnClickListener {
//            for (i in 0..4){
//                if (star[i]==true)    {
//                    requireView().Image.setImageDrawable(
//                        context?.resources?.getDrawable(R.drawable.ic_baseline_star_border_24)
//                    )
//                    star[i] = false
//                }else{
//                    requireView().Image.setImageDrawable(
//                        context?.resources?.getDrawable(R.drawable.ic_baseline_star_24)
//                    )
//                    star[i] = true
//                }
//            }
//        }

        viewModel.goldAndSilverResult.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS) {
                recyclerView.apply {
//                for (i in 1 until 5) {
//                    if (i > 0) {
//                        println("@@@@@@@" + i)
                    var LorH:Boolean=true
                    if(it.data?.silverOns?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.silverOns?.dt ?:null =="low"){
                        LorH=false
                    }

                    noghre.add(
                        items(
                            "انس نقره",
                            it.data?.silverOns?.p ?: null,
                            "ریال",
                            it.data?.silverOns?.d ?: null,
                            "("+it.data?.silverOns?.dp.toString()+")",
                            it.data?.silverOns?.date ?: null,
                            it.data?.silverOns?.time ?: null,
                            LorH,
                            false
                        )
                    )

                    if(it.data?.silver_999?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.silver_999?.dt ?:null =="low"){
                        LorH=false
                    }

                    noghre.add(
                        items(
                            "گرم نقره 999",
                            it.data?.silver_999?.p ?: null,
                            "ریال",
                            it.data?.silver_999?.d ?: null,
                            "("+it.data?.silver_999?.dp.toString()+")",
                            it.data?.silver_999?.date ?: null,
                            it.data?.silver_999?.time ?: null,
                            LorH,
                            false
                        )
                    )

                    if(it.data?.silver_925?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.silver_925?.dt ?:null =="low"){
                        LorH=false
                    }

                    noghre.add(
                        items("گرم نقره 925",
                            it.data?.silver_925?.p ?: null,
                            "ریال",
                            it.data?.silver_925?.d ?: null,
                            "("+it.data?.silver_925?.dp.toString()+")",
                            it.data?.silver_925?.date ?: null,
                            it.data?.silver_925?.time ?: null,
                            LorH,
                            false)
                    )
//            }
//        }
                    adapter = nadapter
                    val manager4 = layoutManager
                    recyclerView.layoutManager = manager4
                    setHasFixedSize(true)
                }
            }
        }
        )

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_noghre, container, false)
//            return binding.root
        return inflater.inflate(R.layout.fragment_noghre, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ViewModelForTala::class.java)
//            binding.also {
////            it.vmt = viewModel
//                it.lifecycleOwner = viewLifecycleOwner
//            }
    }

    fun onClick(myDataItem: items, layoutPosition: Int) {
        nadapter.notifyItemChanged(layoutPosition)
    }

    companion object {
    }
}

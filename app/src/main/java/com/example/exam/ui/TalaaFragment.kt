package com.example  .exam.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.exam.R
import com.example.exam.core.resource.Status
import com.example.exam.databinding.FragmentTalaONoghreBinding
import com.example.exam.models.*
import com.example.exam.ui.MenuClick
import com.example.exam.ui.items
import com.example.exam.ui.talaAdapter
import com.example.exam.viewModel.ViewModelForTala
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.row_my_item.view.*


class TalaONoghreFragment : Fragment() {
//    var star= mutableListOf<Boolean>(false,false,false,false,false)
    lateinit var binding: FragmentTalaONoghreBinding
    lateinit var viewModel: ViewModelForTala
    val tala= mutableListOf<items>()
//    var gold: List<namadLatestItem>? = null
    private val tadapter= talaAdapter(
        this,
        tala,
        this,
        object: MenuClick {
            override fun onItemClick(country: String) {
                TODO("Not yet implemented")
            }
        }
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
//tabdil be recyclerview
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getGoldAndSilver()
//        viewModel.goldAndSilverResult.observe(viewLifecycleOwner, Observer {
//            if (it.status == Status.SUCCESS) {
//                binding.nameValueOns="انس طلای جهانی"
//                binding.priceValueOns= it.data?.ons?.p ?: null
//                binding.dateValueOns= it.data?.ons?.date ?: null
//                binding.changeValueOns= it.data?.ons?.d ?: null
//                binding.changePercentValueOns= "("+it.data?.ons?.dp.toString()+")"
//                if(it.data?.ons?.dt ?:null =="high"){
//                    binding.lorHOns==true
//                } else if(it.data?.ons?.dt ?:null =="low"){
//                    binding.lorHOns==false
//                }
//            }
//        }
//        )


//        view?.Image?.setOnClickListener {
//            for (i in 0..4){
//                if (star[i]==true)    {
//                    requireView().Image.setImageDrawable(
//                        context?.resources?.getDrawable(R.drawable.ic_baseline_star_border_24)
//                    )
//                    star[i] = false
//                }else{
//                    requireView().Image.setImageDrawable(
//                        context?.resources?.getDrawable(R.drawable.ic_baseline_star_24)
//                    )
//                    star[i] = true
//                }
//            }
//        }

    viewModel.goldAndSilverResult.observe(viewLifecycleOwner, Observer {
        if (it.status == Status.SUCCESS) {
            recyclerView.apply {
//                for (i in 1 until 5) {
//                    if (i > 0) {
//                        println("@@@@@@@" + i)
                        var LorH:Boolean=true
                    if(it.data?.ons?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.ons?.dt ?:null =="low"){
                        LorH=false
                    }

                    tala.add(
                    items(
                        "انس طلای جهانی",
                        it.data?.ons?.p ?: null,
                        "دلار",
                        it.data?.ons?.d ?: null,
                        "("+it.data?.ons?.dp.toString()+")",
                        it.data?.ons?.date ?: null,
                        it.data?.ons?.time ?: null,
                        LorH,
                        false
                    )
                )

                if(it.data?.mesghal?.dt ?:null =="high"){
                    LorH=true
                } else if(it.data?.mesghal?.dt ?:null =="low"){
                    LorH=false
                }

                tala.add(
                    items("مثقال",
                        it.data?.mesghal?.p ?: null,
                        "ریال",
                        it.data?.mesghal?.d ?: null,
                        "("+it.data?.mesghal?.dp.toString()+")",
                        it.data?.mesghal?.date ?: null,
                        it.data?.mesghal?.time ?: null,
                        LorH,
                        false)
                )

                if(it.data?.geram18?.dt ?:null =="high"){
                    LorH=true
                } else if(it.data?.geram18?.dt ?:null =="low"){
                    LorH=false
                }

                tala.add(
                    items("گرم طلای 18 عیار",
                        it.data?.geram18?.p ?: null,
                        "ریال",
                        it.data?.geram18?.d ?: null,
                        "("+it.data?.geram18?.dp.toString()+")",
                        it.data?.geram18?.date ?: null,
                        it.data?.geram18?.time ?:null,
                        LorH,
                        false)
                )

                if(it.data?.geram24?.dt ?:null =="high"){
                    LorH=true
                } else if(it.data?.geram24?.dt ?:null =="low"){
                    LorH=false
                }
                tala.add(
                    items("گرم طلای 24 عیار",
                        it.data?.geram24?.p ?: null,
                        "ریال",
                        it.data?.geram24?.d ?: null,
                        "("+it.data?.geram24?.dp.toString()+")",
                        it.data?.geram24?.date ?: null,
                        it.data?.geram24?.time ?: null,
                        LorH,
                        false)
                )
//            }
//        }
                adapter = tadapter
                val manager3 = layoutManager
                recyclerView.layoutManager = manager3
                setHasFixedSize(true)
            }
        }
    }
    )

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tala_o_noghre, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ViewModelForTala::class.java)
        binding.also {
//            it.vmt = viewModel
            it.lifecycleOwner = viewLifecycleOwner
        }
    }

    fun onClick(myDataItem: items, layoutPosition: Int) {
        tadapter.notifyItemChanged(layoutPosition)
    }

    companion object {
    }
}
package com.example.exam.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.exam.R
import com.example.exam.core.resource.Status
import com.example.exam.viewModel.ViewModelForForeignCurrency
import com.example.exam.viewModel.ViewModelForTala
import kotlinx.android.synthetic.main.fragment_home.*


class DollarFragment : Fragment() {
    lateinit var viewModel: ViewModelForForeignCurrency
    val fc= mutableListOf<items>()
    private val fcadapter=DollarAdapter(
        this,
        fc,
        this,
        object:MenuClick{
            override fun onItemClick(country: String) {
                TODO("Not yet implemented")
            }
        }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getForeignCurrency()

        viewModel.foreignCurrencyResult.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS) {
                recyclerView.apply {
//                for (i in 1 until 5) {
//                    if (i > 0) {
//                        println("@@@@@@@" + i)
                    var LorH:Boolean=true
                    if(it.data?.dollar?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.dollar?.dt ?:null =="low"){
                        LorH=false
                    }

                    fc.add(
                        items(
                            "دلار",
                            it.data?.dollar?.p ?: null,
                            "ریال",
                            it.data?.dollar?.d ?: null,
                            "("+it.data?.dollar?.dp.toString()+")",
                            it.data?.dollar?.date ?: null,
                            it.data?.dollar?.time ?: null,
                            LorH,
                            false
                        )
                    )

                    if(it.data?.uro?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.uro?.dt ?:null =="low"){
                        LorH=false
                    }

                    fc.add(
                        items("یورو",
                            it.data?.uro?.p ?: null,
                            "ریال",
                            it.data?.uro?.d ?: null,
                            "("+it.data?.uro?.dp.toString()+")",
                            it.data?.uro?.date ?: null,
                            it.data?.uro?.time ?: null,
                            LorH,
                            false)
                    )

                    if(it.data?.yenJpon?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.yenJpon?.dt ?:null =="low"){
                        LorH=false
                    }

                    fc.add(
                        items("ین ژاپن",
                            it.data?.yenJpon?.p ?: null,
                            "ریال",
                            it.data?.yenJpon?.d ?: null,
                            "("+it.data?.yenJpon?.dp.toString()+")",
                            it.data?.yenJpon?.date ?: null,
                            it.data?.yenJpon?.time ?: null,
                            LorH,
                            false)
                    )

                    if(it.data?.pond?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.pond?.dt ?:null =="low"){
                        LorH=false
                    }

                    fc.add(
                        items("پوند",
                            it.data?.pond?.p ?: null,
                            "ریال",
                            it.data?.pond?.d ?: null,
                            "("+it.data?.pond?.dp.toString()+")",
                            it.data?.pond?.date ?: null,
                            it.data?.pond?.time ?: null,
                            LorH,
                            false)
                    )

                    if(it.data?.dinarKoveit?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.dinarKoveit?.dt ?:null =="low"){
                        LorH=false
                    }

                    fc.add(
                        items("دینار کویت",
                            it.data?.dinarKoveit?.p ?: null,
                            "ریال",
                            it.data?.dinarKoveit?.d ?: null,
                            "("+it.data?.dinarKoveit?.dp.toString()+")",
                            it.data?.dinarKoveit?.date ?: null,
                            it.data?.dinarKoveit?.time ?: null,
                            LorH,
                            false)
                    )

                    if(it.data?.dollarDubai?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.dollarDubai?.dt ?:null =="low"){
                        LorH=false
                    }

                    fc.add(
                        items("دلار دبی",
                            it.data?.dollarDubai?.p ?: null,
                            "ریال",
                            it.data?.dollarDubai?.d ?: null,
                            "("+it.data?.dollarDubai?.dp.toString()+")",
                            it.data?.dollarDubai?.date ?: null,
                            it.data?.dollarDubai?.time ?: null,
                            LorH,
                            false)
                    )

                    if(it.data?.rialQatar?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.rialQatar?.dt ?:null =="low"){
                        LorH=false
                    }

                    fc.add(
                        items("ریال قطر",
                            it.data?.rialQatar?.p ?: null,
                            "ریال",
                            it.data?.rialQatar?.d ?: null,
                            "("+it.data?.rialQatar?.dp.toString()+")",
                            it.data?.rialQatar?.date ?: null,
                            it.data?.rialQatar?.time ?: null,
                            LorH,
                            false)
                    )

                    if(it.data?.dollarNewsLand?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.dollarNewsLand?.dt ?:null =="low"){
                        LorH=false
                    }

                    fc.add(
                        items("دلار نیوزیلند",
                            it.data?.dollarNewsLand?.p ?: null,
                            "ریال",
                            it.data?.dollarNewsLand?.d ?: null,
                            "("+it.data?.dollarNewsLand?.dp.toString()+")",
                            it.data?.dollarNewsLand?.date ?: null,
                            it.data?.dollarNewsLand?.time ?: null,
                            LorH,
                            false)
                    )

                    if(it.data?.dollarSangapur?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.dollarSangapur?.dt ?:null =="low"){
                        LorH=false
                    }

                    fc.add(
                        items("دلار سنگاپور",
                            it.data?.dollarSangapur?.p ?: null,
                            "ریال",
                            it.data?.dollarSangapur?.d ?: null,
                            "("+it.data?.dollarSangapur?.dp.toString()+")",
                            it.data?.dollarSangapur?.date ?: null,
                            it.data?.dollarSangapur?.time ?: null,
                            LorH,
                            false)
                    )
//            }
//        }
                    adapter = fcadapter
                    val manager4 = layoutManager
                    recyclerView.layoutManager = manager4
                    setHasFixedSize(true)
                }
            }
        }
        )

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dollar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ViewModelForForeignCurrency::class.java)
    }

    fun onClick(myDataItem: items, layoutPosition: Int) {
        fcadapter.notifyItemChanged(layoutPosition)
    }
    companion object {
    }
}
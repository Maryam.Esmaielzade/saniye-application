package com.example.exam.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.exam.R
import com.example.exam.core.resource.Status
import com.example.exam.models.Silver925
import com.example.exam.models.Silver999
import com.example.exam.viewModel.ViewModelForArzDigital
import com.example.exam.viewModel.ViewModelForTala
import kotlinx.android.synthetic.main.fragment_home.*


class ArzDigitalFragment : Fragment() {

//    var star= mutableListOf<Boolean>(false,false,false,false,false)
    lateinit var viewModel: ViewModelForArzDigital
    val arz= mutableListOf<items>()
    //    var gold: List<namadLatestItem>? = null
    private val aadapter=ArzAdapter(
        this,
        arz,
        this,
        object:MenuClick{
            override fun onItemClick(country: String) {
                TODO("Not yet implemented")
            }
        }
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getArzDigital()
//        viewModel.goldAndSilverResult.observe(viewLifecycleOwner, Observer {
//            if (it.status == Status.SUCCESS) {
//                binding.nameValueOns="انس طلای جهانی"
//                binding.priceValueOns= it.data?.ons?.p ?: null
//                binding.dateValueOns= it.data?.ons?.date ?: null
//                binding.changeValueOns= it.data?.ons?.d ?: null
//                binding.changePercentValueOns= "("+it.data?.ons?.dp.toString()+")"
//                if(it.data?.ons?.dt ?:null =="high"){
//                    binding.lorHOns==true
//                } else if(it.data?.ons?.dt ?:null =="low"){
//                    binding.lorHOns==false
//                }
//            }
//        }
//        )


//        view?.Image?.setOnClickListener {
//            for (i in 0..4){
//                if (star[i]==true)    {
//                    requireView().Image.setImageDrawable(
//                        context?.resources?.getDrawable(R.drawable.ic_baseline_star_border_24)
//                    )
//                    star[i] = false
//                }else{
//                    requireView().Image.setImageDrawable(
//                        context?.resources?.getDrawable(R.drawable.ic_baseline_star_24)
//                    )
//                    star[i] = true
//                }
//            }
//        }

        viewModel.arzDigitalResult.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS) {
                recyclerView.apply {
//                for (i in 1 until 5) {
//                    if (i > 0) {
//                        println("@@@@@@@" + i)
                    var LorH:Boolean=true
                    if(it.data?.bitcoin?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.bitcoin?.dt ?:null =="low"){
                        LorH=false
                    }

                    arz.add(
                        items(
                            "Bitcoin",
                            it.data?.bitcoin?.p ?: null,
                            "دلار",
                            it.data?.bitcoin?.d ?: null,
                            "("+it.data?.bitcoin?.dp.toString()+")",
                            it.data?.bitcoin?.date ?: null,
                            it.data?.bitcoin?.time ?: null,
                            LorH,
                            false
                        )
                    )

                    if(it.data?.ethereum?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.ethereum?.dt ?:null =="low"){
                        LorH=false
                    }

                    arz.add(
                        items("ٍEthereum",
                            it.data?.ethereum?.p ?: null,
                            "دلار",
                            it.data?.ethereum?.d ?: null,
                            "("+it.data?.ethereum?.dp.toString()+")",
                            it.data?.ethereum?.date ?: null,
                            it.data?.ethereum?.time ?: null,
                            LorH,
                            false)
                    )
                    if(it.data?.tether?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.tether?.dt ?:null =="low"){
                        LorH=false
                    }
                    arz.add(
                        items("Tether",
                            it.data?.tether?.p ?: null,
                            "دلار",
                            it.data?.tether?.d ?: null,
                            "("+it.data?.tether?.dp.toString()+")",
                            it.data?.tether?.date ?: null,
                            it.data?.tether?.time ?: null,
                            LorH,
                            false)
                    )
                    if(it.data?.dash?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.dash?.dt ?:null =="low"){
                        LorH=false
                    }
                    arz.add(
                        items("Dash",
                            it.data?.dash?.p ?: null,
                            "دلار",
                            it.data?.dash?.d ?: null,
                            "("+it.data?.dash?.dp.toString()+")",
                            it.data?.dash?.date ?: null,
                            it.data?.dash?.time ?: null,
                            LorH,
                            false)
                    )
                    if(it.data?.litecoin?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.litecoin?.dt ?:null =="low"){
                        LorH=false
                    }
                    arz.add(
                        items("Litecoin",
                            it.data?.litecoin?.p ?: null,
                            "دلار",
                            it.data?.litecoin?.d ?: null,
                            "("+it.data?.litecoin?.dp.toString()+")",
                            it.data?.litecoin?.date ?: null,
                            it.data?.litecoin?.time ?: null,
                            LorH,
                            false)
                    )
                    if(it.data?.ripple?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.ripple?.dt ?:null =="low"){
                        LorH=false
                    }
                    arz.add(
                        items("Ripple",
                            it.data?.ripple?.p ?: null,
                            "دلار",
                            it.data?.ripple?.d ?: null,
                            "("+it.data?.ripple?.dp.toString()+")",
                            it.data?.ripple?.date ?: null,
                            it.data?.ripple?.time ?: null,
                            LorH,
                            false)
                    )
                    if(it.data?.stellar?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.stellar?.dt ?:null =="low"){
                        LorH=false
                    }
                    arz.add(
                        items("Stellar",
                            it.data?.stellar?.p ?: null,
                            "دلار",
                            it.data?.stellar?.d ?: null,
                            "("+it.data?.stellar?.dp.toString()+")",
                            it.data?.stellar?.date ?: null,
                            it.data?.stellar?.time ?: null,
                            LorH,
                            false)
                    )
                    if(it.data?.eos?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.eos?.dt ?:null =="low"){
                        LorH=false
                    }
                    arz.add(
                        items("Eos",
                            it.data?.eos?.p ?: null,
                            "دلار",
                            it.data?.eos?.d ?: null,
                            "("+it.data?.eos?.dp.toString()+")",
                            it.data?.eos?.date ?: null,
                            it.data?.eos?.time ?: null,
                            LorH,
                            false)
                    )
//            }
//        }
                    adapter = aadapter
                    val manager7 = layoutManager
                    recyclerView.layoutManager = manager7
                    setHasFixedSize(true)
                }
            }
        }
        )

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_noghre, container, false)
//            return binding.root
        return inflater.inflate(R.layout.fragment_arz_digital, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ViewModelForArzDigital::class.java)
//            binding.also {
////            it.vmt = viewModel
//                it.lifecycleOwner = viewLifecycleOwner
//            }
    }

    fun onClick(myDataItem: items, layoutPosition: Int) {
        aadapter.notifyItemChanged(layoutPosition)
    }

    companion object {
    }
}
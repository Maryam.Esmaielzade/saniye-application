package com.example.exam.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.data.Set
import com.anychart.enums.Anchor
import com.anychart.enums.MarkerType
import com.anychart.enums.TooltipPositionMode
import com.anychart.graphics.vector.Stroke
import com.example.exam.R
import com.example.exam.core.resource.Status
import com.example.exam.data.repository1.repository
import com.example.exam.databinding.FragmentHomeBinding
import com.example.exam.models.namadArchiveItem
import com.example.exam.models.namadLatestItem
import com.example.exam.viewModel.ViewModelForHome
import com.example.exam.viewModel.ViewModelForOneNamad
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_one_country.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import java.util.Observer

class OneNamadFragment : Fragment() {
    lateinit var viewModel: ViewModelForOneNamad
    var namads: List<namadArchiveItem>? = null
//    lateinit var binding: FragmentOneNamadBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val anyChartView: AnyChartView = any_chart_view
        anyChartView.setProgressBar(progress_bar)
        val cartesian = AnyChart.line()
        viewModel.getNamadArchive()
        cartesian.animation(true)
        cartesian.padding(10.0, 20.0, 5.0, 20.0)
        cartesian.crosshair().enabled(true)
        cartesian.crosshair()
            .yLabel(true) // TODO ystroke
            .yStroke(
                null as Stroke?,
                null,
                null,
                null as String?,
                null as String?
            )
        cartesian.tooltip().positionMode(TooltipPositionMode.POINT)
        cartesian.title("Trend of Sales of the Most Popular Products of ACME Corp.")
        cartesian.yAxis(0).title("Number of Bottles Sold (thousands)")
        cartesian.xAxis(0).labels().padding(5.0, 5.0, 5.0, 5.0)
        val seriesData: MutableList<DataEntry> = ArrayList()
        lifecycleScope.launch(Dispatchers.IO) {
            viewModel.namadResult.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                if (it.status == Status.SUCCESS) {
                    namads = it.data
                    for (i in 1 until 10) {
                        if (i > 0) {
                            println(
                                "@@@@@@@" + i + "---" + namads?.get(i)?.date + "---" + namads?.get(
                                    i
                                )?.finalPrice
                            )
                            seriesData.add(
                                CustomDataEntry(
                                    namads?.get(i)?.date + i,
                                    namads?.get(i)?.finalPrice
                                )
                            )
                        }
                    }
                }
            })
        }
//        seriesData.add(CustomDataEntry("1986", 3.6))
//        seriesData.add(CustomDataEntry("1987", 7.1))
//        seriesData.add(CustomDataEntry("1988", 8.5))
//        seriesData.add(CustomDataEntry("1989", 9.2))
//        seriesData.add(CustomDataEntry("1990", 10.1))
//        seriesData.add(CustomDataEntry("1991", 11.6))
//        seriesData.add(CustomDataEntry("1992", 16.4))
//        seriesData.add(CustomDataEntry("1993", 18.0))
//        seriesData.add(CustomDataEntry("1994", 13.2))
//        seriesData.add(CustomDataEntry("1995", 12.0))
//        seriesData.add(CustomDataEntry("1996", 3.2))
        val set = Set.instantiate()
        set.data(seriesData)
        val series1Mapping = set.mapAs("{ x: 'x', value: 'value' }")
        //        Mapping series2Mapping = set.mapAs("{ x: 'x', value: 'value2' }");
//        Mapping series3Mapping = set.mapAs("{ x: 'x', value: 'value3' }");
        val series1 = cartesian.line(series1Mapping)
        series1.name("Brandy")
        series1.hovered().markers().enabled(true)
        series1.hovered().markers()
            .type(MarkerType.CIRCLE)
            .size(4.0)
        series1.tooltip()
            .position("right")
            .anchor(Anchor.LEFT_CENTER)
            .offsetX(5.0)
            .offsetY(5.0)

//        Line series2 = cartesian.line(series2Mapping);
//        series2.name("Whiskey");
//        series2.hovered().markers().enabled(true);
//        series2.hovered().markers()
//                .type(MarkerType.CIRCLE)
//                .size(4d);
//        series2.tooltip()
//                .position("right")
//                .anchor(Anchor.LEFT_CENTER)
//                .offsetX(5d)
//                .offsetY(5d);
//
//        Line series3 = cartesian.line(series3Mapping);
//        series3.name("Tequila");
//        series3.hovered().markers().enabled(true);
//        series3.hovered().markers()
//                .type(MarkerType.CIRCLE)
//                .size(4d);
//        series3.tooltip()
//                .position("right")
//                .anchor(Anchor.LEFT_CENTER)
//                .offsetX(5d)
//                .offsetY(5d);
        cartesian.legend().enabled(true)
        cartesian.legend().fontSize(13.0)
        cartesian.legend().padding(0.0, 0.0, 10.0, 0.0)
        anyChartView.setChart(cartesian)
    }
    private inner class CustomDataEntry internal constructor(
        x: String?,
        value: Number? /*, Number value2, Number value3*/
    ) :
        ValueDataEntry(x, value)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_one_country, container, false)
//        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
//        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val repository = repository()
        viewModel = ViewModelProvider(this).get(ViewModelForOneNamad::class.java)
//        binding.also {
//            it.vm = viewModel
//            it.lifecycleOwner = viewLifecycleOwner
//        }
    }

    companion object {

    }
}

private fun <T> MutableLiveData<T>.observe(viewLifecycleOwner: LifecycleOwner, observer: Observer) {

}

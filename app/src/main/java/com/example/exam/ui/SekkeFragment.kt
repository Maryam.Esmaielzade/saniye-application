package com.example.exam.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.exam.R
import com.example.exam.core.resource.Status
import com.example.exam.models.Silver925
import com.example.exam.models.Silver999
import com.example.exam.viewModel.ViewModelForTala
import kotlinx.android.synthetic.main.fragment_home.*


class SekkeFragment : Fragment() {
//    var star= mutableListOf<Boolean>(false,false,false,false,false)
    lateinit var viewModel: ViewModelForTala
    val sekke= mutableListOf<items>()
    //    var gold: List<namadLatestItem>? = null
    private val sadapter=SekkeAdapter(
        this,
        sekke,
        this,
        object:MenuClick{
            override fun onItemClick(country: String) {
                TODO("Not yet implemented")
            }
        }
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getGoldAndSilver()
//        viewModel.goldAndSilverResult.observe(viewLifecycleOwner, Observer {
//            if (it.status == Status.SUCCESS) {
//                binding.nameValueOns="انس طلای جهانی"
//                binding.priceValueOns= it.data?.ons?.p ?: null
//                binding.dateValueOns= it.data?.ons?.date ?: null
//                binding.changeValueOns= it.data?.ons?.d ?: null
//                binding.changePercentValueOns= "("+it.data?.ons?.dp.toString()+")"
//                if(it.data?.ons?.dt ?:null =="high"){
//                    binding.lorHOns==true
//                } else if(it.data?.ons?.dt ?:null =="low"){
//                    binding.lorHOns==false
//                }
//            }
//        }
//        )


//        view?.Image?.setOnClickListener {
//            for (i in 0..4){
//                if (star[i]==true)    {
//                    requireView().Image.setImageDrawable(
//                        context?.resources?.getDrawable(R.drawable.ic_baseline_star_border_24)
//                    )
//                    star[i] = false
//                }else{
//                    requireView().Image.setImageDrawable(
//                        context?.resources?.getDrawable(R.drawable.ic_baseline_star_24)
//                    )
//                    star[i] = true
//                }
//            }
//        }

        viewModel.goldAndSilverResult.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS) {
                recyclerView.apply {
//                for (i in 1 until 5) {
//                    if (i > 0) {
//                        println("@@@@@@@" + i)
                    var LorH:Boolean=true
                    if(it.data?.sekee?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.sekee?.dt ?:null =="low"){
                        LorH=false
                    }

                    sekke.add(
                        items(
                            "سکه",
                            it.data?.sekee?.p ?: null,
                            "ریال",
                            it.data?.sekee?.d ?: null,
                            "("+it.data?.sekee?.dp.toString()+")",
                            it.data?.sekee?.date ?: null,
                            it.data?.sekee?.time ?: null,
                            LorH,
                            false
                        )
                    )

                    if(it.data?.nimSekee?.dt ?:null =="high"){
                        LorH=true
                    } else if(it.data?.nimSekee?.dt ?:null =="low"){
                        LorH=false
                    }

                    sekke.add(
                        items("نیم سکه",
                            it.data?.nimSekee?.p ?: null,
                            "ریال",
                            it.data?.nimSekee?.d ?: null,
                            "("+it.data?.nimSekee?.dp.toString()+")",
                            it.data?.nimSekee?.date ?: null,
                            it.data?.nimSekee?.time ?: null,
                            LorH,
                            false)
                    )
                    sekke.add(
                        items("ربع سکه",
                            it.data?.robSekee?.p ?: null,
                            "ریال",
                            it.data?.robSekee?.d ?: null,
                            "("+it.data?.robSekee?.dp.toString()+")",
                            it.data?.robSekee?.date ?: null,
                            it.data?.robSekee?.time ?: null,
                            LorH,
                            false)
                    )
//            }
//        }
                    adapter = sadapter
                    val manager5 = layoutManager
                    recyclerView.layoutManager = manager5
                    setHasFixedSize(true)
                }
            }
        }
        )

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_noghre, container, false)
//            return binding.root
        return inflater.inflate(R.layout.fragment_sekke, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ViewModelForTala::class.java)
//            binding.also {
////            it.vmt = viewModel
//                it.lifecycleOwner = viewLifecycleOwner
//            }
    }

    fun onClick(myDataItem: items, layoutPosition: Int) {
        sadapter.notifyItemChanged(layoutPosition)
    }

    companion object {
    }
}
package com.example.exam.models

data class Tether(
    val d: String,
    val dp: Double,
    val dt: String,
    val h: String,
    val l: String,
    val p: String,
    val t: String,
    val t_g: String,
    val t_en: String,
    val ts: String,
    val time: String?,
    val date: String?
)
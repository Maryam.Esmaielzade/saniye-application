package com.example.exam.models

data class DollarNewsLand(
    val d: String,
    val date: String,
    val dp: Double,
    val dt: String,
    val h: String,
    val l: String,
    val p: String,
    val t: String,
    val t_g: String,
    val t_en: String,
    val time: String,
    val ts: String
)
package com.example.exam.models

data class arzDigital(
    val bitcoin: Bitcoin,
    val dash: Dash,
    val eos: Eos,
    val ethereum: Ethereum,
    val litecoin: Litecoin,
    val ripple: Ripple,
    val stellar: Stellar,
    val tether: Tether
)
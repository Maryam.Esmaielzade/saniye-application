package com.example.exam.models

data class StockGainItem(
    val afterRaisingStockPrice: Int,
    val beforeRaisingStockPrice: Int,
    val raisingStockDate: String,
    val symbol: String,
    val symbolId: String,
    val todayPrice: Int
)
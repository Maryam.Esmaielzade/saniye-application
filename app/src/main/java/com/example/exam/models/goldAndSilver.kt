package com.example.exam.models

data class goldAndSilver(
    val geram18: Geram18,
    val geram24: Geram24,
    val mesghal: Mesghal,
    val nimSekee: NimSekee,
    val ons: Ons,
    val robSekee: RobSekee,
    val sekee: Sekee,
    val silver_925: Silver925,
    val silver_999: Silver999,
    val silverOns: SilverOns
)
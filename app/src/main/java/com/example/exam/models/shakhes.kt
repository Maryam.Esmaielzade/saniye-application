package com.example.exam.models

data class shakhes(
    val arzeshBazar: String,
    val arzeshMoamela: String,
    val date: String,
    val hajemMoamelat: String,
    val shakhesHamVazn: String,
    val shakhesKol: String,
    val tedadeMoamlat: String,
    val time: String
)
package com.example.exam.models

data class ForeignCurrency(
    val dinarKoveit: DinarKoveit,
    val dollar: Dollar,
    val dollarDubai: DollarDubai,
    val dollarNewsLand: DollarNewsLand,
    val dollarSangapur: DollarSangapur,
    val pond: Pond,
    val rialQatar: RialQatar,
    val uro: Uro,
    val yenJpon: YenJpon
)
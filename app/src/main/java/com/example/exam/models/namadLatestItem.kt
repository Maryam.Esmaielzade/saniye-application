package com.example.exam.models

data class namadLatestItem(
    val changePrice: Double,
    val cost: Long,
    val date: String,
    val finalPrice: Int,
    val firstPrice: Int,
    val lastTransactionPrice: Int,
    val maxPrice: Int,
    val minPrice: Int,
    val number: Int,
    val symbol: String,
    val symbolId: String,
    val time: String,
    val volume: Int,
    val yesterdayPrice: Int
)
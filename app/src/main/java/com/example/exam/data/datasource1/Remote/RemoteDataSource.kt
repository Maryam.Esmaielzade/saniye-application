package com.example.exam.data.datasource1.Remote

import com.example.exam.core.safeApiCall
import com.example.exam.services.API
import com.example.exam.services.RetrofitService

class RemoteDataSource {
    var myApi: API = RetrofitService.retrofit.create(API::class.java)
    var api: API = RetrofitService.retrofit.create(API::class.java)

//    suspend fun getAll()=safeApiCall {
//        myApi.getAll()
//    }
//    suspend fun getAllCountries()=safeApiCall {
//        myApi.getAllCountries()
//    }
    suspend fun getNamadLatest()= safeApiCall {
        myApi.getNamadLatest()
    }

    suspend fun getNamadLatestWithID(id:String)= safeApiCall {
        myApi.getNamadLatestWithID(id)
    }

    suspend fun getStockGainWithID()= safeApiCall {
        myApi.getStockGainWithID()
    }

    suspend fun getNamadArchive()= safeApiCall {
        api.getNamadArchive()
    }

    suspend fun getShakhes()= safeApiCall {
        api.getShakhes()
    }
    suspend fun getGoldAndSilver()= safeApiCall {
        api.getGoldAndSilver()
    }

    suspend fun getArzDigital()= safeApiCall {
        api.getArzDigital()
    }

    suspend fun getForeignCurrency()= safeApiCall {
        api.getForeignCurrency()
    }

//    suspend fun getStockGain()= safeApiCall {
//        api.getStockGain()
//    }
}
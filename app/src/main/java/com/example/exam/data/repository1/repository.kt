package com.example.exam.data.repository1

import com.example.exam.core.resource.Resource
import com.example.exam.core.resource.Status
import com.example.exam.data.datasource1.Remote.RemoteDataSource
import com.example.exam.models.*
//import com.example.exam.models.All
//import com.example.exam.models.AllCountries

class repository {
    private val remote=RemoteDataSource()
//    suspend fun getAll(): Resource<All> {
//
//        val resource = remote.getAll()
//        if (resource.status == Status.SUCCESS) {
//            var AllInfo  =resource.data
//            println(AllInfo)
//        }
//        return resource
//    }
//    suspend fun getAllCountries(): Resource<AllCountries> {
//
//        val resource = remote.getAllCountries()
//        if (resource.status == Status.SUCCESS) {
//            println(resource.data)
//        }
//        return resource
//    }
    suspend fun getNamadLatest(): Resource<namadLatest> {

        val resource = remote.getNamadLatest()
        if (resource.status == Status.SUCCESS) {
            println(resource.data)
        }
        return resource
    }

    suspend fun getNamadLatestWithID(id:String): Resource<namadLatestItem> {

        val resource0 = remote.getNamadLatestWithID(id)
        if (resource0.status == Status.SUCCESS) {
            println(resource0.data)
        }
        return resource0
    }

    suspend fun getStockGainWithID(): Resource<stockGain> {

        val resourc = remote.getStockGainWithID()
        if (resourc.status == Status.SUCCESS) {
            println(resourc.data)
        }
        return resourc
    }


    suspend fun getNamadArchive(): Resource<namadArchive> {

        val resource1 = remote.getNamadArchive()
        if (resource1.status == Status.SUCCESS) {
            println(resource1.data)
        }
        return resource1
    }

    suspend fun getShakhes(): Resource<shakhes>{
        val resource2 = remote.getShakhes()
        if (resource2.status == Status.SUCCESS) {
            println(resource2.data)
        }
        return resource2
    }
    suspend fun getGoldAndSilver(): Resource<goldAndSilver>{
        val resource3 = remote.getGoldAndSilver()
        if (resource3.status == Status.SUCCESS) {
            println(resource3.data)
        }
        return resource3
    }
    suspend fun getarzDigital(): Resource<arzDigital>{
        val resource4 = remote.getArzDigital()
        if (resource4.status == Status.SUCCESS) {
            println(resource4.data)
        }
        return resource4
    }

    suspend fun getForeignCurrency(): Resource<ForeignCurrency>? {
        val resource5 = remote.getForeignCurrency()
        if (resource5.status == Status.SUCCESS) {
            println(resource5.data)
        }
        return resource5
    }
}
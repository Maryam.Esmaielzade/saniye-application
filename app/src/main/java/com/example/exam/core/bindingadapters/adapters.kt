package com.example.exam.core.bindingadapters

import android.view.View
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import com.example.exam.core.resource.Resource
import com.example.exam.core.resource.Status


@BindingAdapter("app:visibleOnResult")
fun View.visibleOnResult(resource: Resource<*>?) {
    isVisible = resource?.status == Status.LOADING
}
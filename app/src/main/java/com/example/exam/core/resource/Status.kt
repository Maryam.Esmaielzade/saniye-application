package com.example.exam.core.resource

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
